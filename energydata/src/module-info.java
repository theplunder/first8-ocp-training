
module energydata {
    exports nl.first8.ocpklas.energydata.api;

    exports nl.first8.ocpklas.energydata.databron;
    exports nl.first8.ocpklas.energydata.domain;

    requires transitive transitives;
}