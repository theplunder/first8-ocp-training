package nl.first8.ocpklas.energydata.databron;

import nl.first8.ocpklas.energydata.domain.EnergieAanbodEntry;

import java.util.List;
import java.util.ServiceLoader;

public interface EnergyAanbodProvider {


    List<EnergieAanbodEntry> provide();


}
