package nl.first8.ocpklas.energydata.domain;

import java.time.Year;

public record AanbodPeriode(Year year, PeriodeType periodeType, int periodeNummer) {
}
