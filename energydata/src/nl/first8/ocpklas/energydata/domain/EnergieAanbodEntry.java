package nl.first8.ocpklas.energydata.domain;

public class EnergieAanbodEntry {

    /* "ID";"Perioden";"BrutoProductie_1";"EigenVerbruikBijElektrProductie_2";"NettoProductieTotaal_3";
     * "Kernenergie_4";"BrandstoffenTotaal_5";"Kolen_6";"Olieproducten_7";"Aardgas_8";"Biomassa_9";
     * "OverigeBrandstoffenNietHernieuwbaar_10";"Waterkracht_11";"WindenergieTotaal_12";"WindenergieOpLand_13";
     * "WindenergieOpZee_14";"Zonnestroom_15";"OverigeBronnen_16";"InvoerTotaal_17";"Belgie_18";
     * "Denemarken_19";"Duitsland_20";"VerenigdKoninkrijk_21";"Noorwegen_22";"UitvoerTotaal_23";
     * "Belgie_24";"Denemarken_25";"Duitsland_26";"VerenigdKoninkrijk_27";"Noorwegen_28";
     * "Distributieverliezen_29";"NettoVerbruikBerekend_30"
     */

    //TODO de import en export van energie naar het buitenland zit er nog niet in.
    // Kan je dat misschien mbv een Record en/of EnumMap toevoegen??



    private final String identifier;
    private final AanbodPeriode aanbodPeriode;

    //"BrutoProductie_1"
    private int brutoProductie;
    //"EigenVerbruikBijElektrProductie_2"
    private int eigenVerbruikBijElektrProductie;
    //"NettoProductieTotaal_3"
    private int nettoProductieTotaal;
    //"Kernenergie_4"
    private int kernenergie;
    //"BrandstoffenTotaal_5"
    private int brandstoffenTotaal;
    //"Kolen_6"
    private int kolen;
    //"Olieproducten_7"
    private int olieproducten;
    //"Aardgas_8"
    private int aardgas;
    //"Biomassa_9"
    private int biomassa;
    //"OverigeBrandstoffenNietHernieuwbaar_10"
    private int overigeBrandstoffenNietHernieuwbaar;
    //"Waterkracht_11"
    private int waterkracht;
    //"WindenergieTotaal_12"
    private int windenergieTotaal;
    //"WindenergieOpLand_13"
    private int windenergieOpLand;
    //"WindenergieOpZee_14"
    private int windenergieOpZee;
    //"Zonnestroom_15"
    private int zonnestroom;
    //"OverigeBronnen_16"
    private int overigeBronnen;
    //"InvoerTotaal_17"
    private int invoerTotaal;
    //"UitvoerTotaal_23"
    private int uitvoerTotaal;
    //"Distributieverliezen_29"
    private int distributieverliezen;
    //"NettoVerbruikBerekend_30"
    private int nettoVerbruikBerekend;

    public EnergieAanbodEntry(String identifier, AanbodPeriode aanbodPeriode) {
        this.identifier = identifier;
        this.aanbodPeriode = aanbodPeriode;
    }

    public String getIdentifier() {
        return identifier;
    }

    public AanbodPeriode getAanbodPeriode() {
        return aanbodPeriode;
    }

    public int getBrutoProductie() {
        return brutoProductie;
    }

    public void setBrutoProductie(int brutoProductie) {
        this.brutoProductie = brutoProductie;
    }

    public int getEigenVerbruikBijElektrProductie() {
        return eigenVerbruikBijElektrProductie;
    }

    public void setEigenVerbruikBijElektrProductie(int eigenVerbruikBijElektrProductie) {
        this.eigenVerbruikBijElektrProductie = eigenVerbruikBijElektrProductie;
    }

    public int getNettoProductieTotaal() {
        return nettoProductieTotaal;
    }

    public void setNettoProductieTotaal(int nettoProductieTotaal) {
        this.nettoProductieTotaal = nettoProductieTotaal;
    }

    public int getKernenergie() {
        return kernenergie;
    }

    public void setKernenergie(int kernenergie) {
        this.kernenergie = kernenergie;
    }

    public int getBrandstoffenTotaal() {
        return brandstoffenTotaal;
    }

    public void setBrandstoffenTotaal(int brandstoffenTotaal) {
        this.brandstoffenTotaal = brandstoffenTotaal;
    }

    public int getKolen() {
        return kolen;
    }

    public void setKolen(int kolen) {
        this.kolen = kolen;
    }

    public int getOlieproducten() {
        return olieproducten;
    }

    public void setOlieproducten(int olieproducten) {
        this.olieproducten = olieproducten;
    }

    public int getAardgas() {
        return aardgas;
    }

    public void setAardgas(int aardgas) {
        this.aardgas = aardgas;
    }

    public int getBiomassa() {
        return biomassa;
    }

    public void setBiomassa(int biomassa) {
        this.biomassa = biomassa;
    }

    public int getOverigeBrandstoffenNietHernieuwbaar() {
        return overigeBrandstoffenNietHernieuwbaar;
    }

    public void setOverigeBrandstoffenNietHernieuwbaar(int overigeBrandstoffenNietHernieuwbaar) {
        this.overigeBrandstoffenNietHernieuwbaar = overigeBrandstoffenNietHernieuwbaar;
    }

    public int getWaterkracht() {
        return waterkracht;
    }

    public void setWaterkracht(int waterkracht) {
        this.waterkracht = waterkracht;
    }

    public int getWindenergieTotaal() {
        return windenergieTotaal;
    }

    public void setWindenergieTotaal(int windenergieTotaal) {
        this.windenergieTotaal = windenergieTotaal;
    }

    public int getWindenergieOpLand() {
        return windenergieOpLand;
    }

    public void setWindenergieOpLand(int windenergieOpLand) {
        this.windenergieOpLand = windenergieOpLand;
    }

    public int getWindenergieOpZee() {
        return windenergieOpZee;
    }

    public void setWindenergieOpZee(int windenergieOpZee) {
        this.windenergieOpZee = windenergieOpZee;
    }

    public int getZonnestroom() {
        return zonnestroom;
    }

    public void setZonnestroom(int zonnestroom) {
        this.zonnestroom = zonnestroom;
    }

    public int getOverigeBronnen() {
        return overigeBronnen;
    }

    public void setOverigeBronnen(int overigeBronnen) {
        this.overigeBronnen = overigeBronnen;
    }

    public int getInvoerTotaal() {
        return invoerTotaal;
    }

    public void setInvoerTotaal(int invoerTotaal) {
        this.invoerTotaal = invoerTotaal;
    }

    public int getUitvoerTotaal() {
        return uitvoerTotaal;
    }

    public void setUitvoerTotaal(int uitvoerTotaal) {
        this.uitvoerTotaal = uitvoerTotaal;
    }

    public int getDistributieverliezen() {
        return distributieverliezen;
    }

    public void setDistributieverliezen(int distributieverliezen) {
        this.distributieverliezen = distributieverliezen;
    }

    public int getNettoVerbruikBerekend() {
        return nettoVerbruikBerekend;
    }

    public void setNettoVerbruikBerekend(int nettoVerbruikBerekend) {
        this.nettoVerbruikBerekend = nettoVerbruikBerekend;
    }

    @Override
    public String toString() {
        return "EnergieAanbodEntry{" +
                "identifier='" + identifier + '\'' +
                ", aanbodPeriode=" + aanbodPeriode +
                '}';
    }
}
