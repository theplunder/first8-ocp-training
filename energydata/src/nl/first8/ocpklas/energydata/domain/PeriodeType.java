package nl.first8.ocpklas.energydata.domain;

public enum PeriodeType {
    JAAR(12), KWARTAAL(3), MAAND(1);

    private int aantalMaanden;

    PeriodeType(int aantalMaanden) {
        this.aantalMaanden = aantalMaanden;
    }

    public int aantalMaanden() {
        return aantalMaanden;
    }


}
