package nl.first8.ocpklas.energydata.internals;

//this class should not be accessible from outside
public class InternalStuff {


    public void printMe(){
        Class<?> caller = StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE).getCallerClass();
        System.out.println("I was accessed  from  " + caller.getName());
    }

}
