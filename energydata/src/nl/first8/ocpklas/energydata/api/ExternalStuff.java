package nl.first8.ocpklas.energydata.api;

public class ExternalStuff {

    public void printMe(){
        Class<?> caller = StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE).getCallerClass();
        System.out.println("I was accessed  from  " + caller.getName());
    }

}
