package nl.first8.ocpklas.energydata.main;

import nl.first8.ocpklas.energydata.internals.InternalStuff;

public class Main {


    public static void main(String[] args) {
        var internal = new InternalStuff();
        internal.printMe();
    }
}
