import nl.first8.ocpklas.meter.PerformanceMeter;

public class Main {


    public static void main(String[] args) throws InterruptedException {

        var duration = PerformanceMeter.measure(100, 1000, () -> System.out.println("Hello world!"));

        System.out.println("It took " + duration.toMillis() + " ms");
        System.out.println("It took " + duration.toNanos() + " ns");
    }





}


