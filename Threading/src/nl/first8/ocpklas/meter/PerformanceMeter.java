package nl.first8.ocpklas.meter;

import java.time.Duration;

public class PerformanceMeter {


    public static Duration measure(int cycles, int warmupCycles, Operation operation) throws InterruptedException {

        for (int i = 0; i < warmupCycles; i++) {
            operation.start();
        }

        System.out.println("done with warmup");

        long start = System.nanoTime();

        for (int i = 0; i < cycles; i++) {
            operation.start();
        }
        long end = System.nanoTime();

        return Duration.ofNanos(end - start);
    }


}
