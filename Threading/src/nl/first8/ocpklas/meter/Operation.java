package nl.first8.ocpklas.meter;

@FunctionalInterface
public interface Operation {

    void start() throws InterruptedException;

}
