package nl.first8.ocpklas.threading.racing;

import nl.first8.ocpklas.meter.PerformanceMeter;

public class CostOfSync {

    public static void main(String[] args) throws InterruptedException {
        syncExamples();

        System.out.println();
        System.out.println("========== Dividing the work ============");

        dividingTheWork(1);
        dividingTheWork(2);
        dividingTheWork(4);
        dividingTheWork(10);
        dividingTheWork(100);
    }

    private static void syncExamples() throws InterruptedException {
        System.out.println("========== SyncExamples::withoutMultithreading ============");
        var duration0 = PerformanceMeter.measure(1000, 100, SyncExamples::withoutMultithreading);
        System.out.println("It took " + duration0.toMillis() + " ms");
        System.out.println("It took " + duration0.toNanos() + " ns");

        System.out.println("========== SyncExamples::syncBlock ============");
        var duration1 = PerformanceMeter.measure(1000, 100, SyncExamples::syncBlock);
        System.out.println("It took " + duration1.toMillis() + " ms");
        System.out.println("It took " + duration1.toNanos() + " ns");

        System.out.println("========== SyncExamples:: syncMethod============");
        var duration2 = PerformanceMeter.measure(1000, 100, SyncExamples::syncMethod);
        System.out.println("It took " + duration2.toMillis() + " ms");
        System.out.println("It took " + duration2.toNanos() + " ns");

        System.out.println("========== SyncExamples::atomicSumming ============");
        var duration3 = PerformanceMeter.measure(1000, 100, SyncExamples::atomicSumming);
        System.out.println("It took " + duration3.toMillis() + " ms");
        System.out.println("It took " + duration3.toNanos() + " ns");
    }

    private static void dividingTheWork(int n) throws InterruptedException {
        System.out.println("========== Dividing:: " + n + "  ============");
        var duration = PerformanceMeter.measure(1000, 100, () -> DividingTheWork.sumItWithNThreads(n));
        System.out.println("It took " + duration.toMillis() + " ms");
        System.out.println("It took " + duration.toNanos() + " ns");
    }
}
