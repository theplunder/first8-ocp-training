package nl.first8.ocpklas.threading.racing;

import java.util.concurrent.atomic.AtomicInteger;

public class SyncExamples {

    private static int myMutable = 0;

    private static int myOtherMutable = 0;

    private static int anotherMutable = 0;

    private static AtomicInteger myAtomic = new AtomicInteger(0);

    private static final Object LOCK = new Object();

    private static final int sumMax = 1_000_000;

    public static void main(String[] args) throws InterruptedException {
//        syncBlock();
//        System.out.println("syncBlock -> Expected: " + (2 * sumMax) + " and was: " + myMutable);

//        syncMethod();
//        System.out.println("syncMethod -> Expected: " + (2 * sumMax) + " and was: " + myOtherMutable);
//
//        atomicSumming();
//        System.out.println("atomicSumming -> Expected: " + (2 * sumMax) + " and was: " + myAtomic.get());

    }

    public static void syncBlock() throws InterruptedException {
        myMutable = 0;
        Runnable operation = () -> sumMyMutableWithSync(sumMax);

        doItWith2Threads(operation);
    }

    public static void syncMethod() throws InterruptedException {
        myOtherMutable = 0;
        Runnable operation = () -> sumMyMutableWithSyncMethod(sumMax);

        doItWith2Threads(operation);
    }

    public static void atomicSumming() throws InterruptedException {
        myAtomic = new AtomicInteger(0);
        Runnable operation = () -> sumMyAtomic(sumMax);

        doItWith2Threads(operation);
    }


    public static void withoutMultithreading() {
        anotherMutable = 0;
        Runnable operation = () -> {
            for (int i = 0; i < sumMax * 2; i++) {
                anotherMutable++;
            }
        };
        operation.run();
    }


    private static void doItWith2Threads(Runnable operation) throws InterruptedException {
        Thread thread1 = new Thread(operation);
        Thread thread2 = new Thread(operation);

        thread1.start();
        thread2.start();

        //wait untill threads are finished
        thread1.join();
        thread2.join();
    }

    private static void sumMyMutableWithSync(int times) {
        for (int i = 0; i < times; i++) {
            synchronized (LOCK) {
                myMutable++;
            }
        }
    }

    private static void sumMyAtomic(int times) {
        for (int i = 0; i < times; i++) {
            int _ = myAtomic.incrementAndGet(); //variable with name _ is preview-feature
        }
    }

    private static void sumMyMutableWithSyncMethod(int times) {
        for (int i = 0; i < times; i++) {
            syncedSum();
        }
    }

    public static synchronized void syncedSum() {
        myOtherMutable++;
    }


}
