package nl.first8.ocpklas.threading.racing;

public class RacingExamples {

    private static int myMutable = 0;

    //Volatile does NOT solve this problem!
    private static volatile int myVolatile = 0;


    public static void main(String[] args) throws InterruptedException {
        theWorstCase();
        somethingWithVolatile();
    }

    private static void theWorstCase() throws InterruptedException {
        int sumMax = 100_000;
        Runnable operation = () -> sumMyMutable(sumMax);

        doItWith2Threads(operation);

        System.out.println("theWorstCase -> Expected: " + (2 * sumMax) + " but was: " + myMutable);
    }


    private static void somethingWithVolatile() throws InterruptedException {
        int sumMax = 100000;
        Runnable operation = () -> sumMyVolatile(sumMax);

        doItWith2Threads(operation);

        System.out.println("somethingWithVolatile -> Expected: " + (2 * sumMax) + " but was: " + myVolatile);
    }



    private static void doItWith2Threads(Runnable operation) throws InterruptedException {
        Thread thread1 = new Thread(operation);
        Thread thread2 = new Thread(operation);

        thread1.start();
        thread2.start();

        //wait untill threads are finished
        thread1.join();
        thread2.join();
    }

    private static void sumMyMutable(int times) {
        for (int i = 0; i < times; i++) {
            myMutable++;
        }
    }

    private static void sumMyVolatile(int times) {
        for (int i = 0; i < times; i++) {
            myVolatile++;
        }
    }
}
