package nl.first8.ocpklas.threading.racing;

import java.util.ArrayList;
import java.util.List;

public class DividingTheWork {

    private static int myMutable = 0;
    private static final int sumMax = 2_000_000;

    public static class MySummer implements Runnable {

        private int currentval = 0;
        private int until;

        MySummer(int until) {
            this.until = until;
        }

        @Override
        public void run() {
            for (int i = 0; i < until; i++) {
                currentval++;
            }
        }

        public int reportValue() {
            return currentval;
        }
    }


    public static void main(String[] args) throws InterruptedException {
        sumItWithNThreads(2);
        System.out.println("2 threads Result is --> " + myMutable);

        sumItWithNThreads(4);
        System.out.println("4 threads Result is --> " + myMutable);


        sumItWithNThreads(10000);
        System.out.println("1000 threads Result is --> " + myMutable);

    }


    //precondition, sumMax should be a multiple of threadCount
    public static void sumItWithNThreads(int threadCout) {

        int limit = sumMax / threadCout;

        List<MySummer> summers = new ArrayList<>();
        for (int i = 0; i < threadCout; i++) {
            summers.add(new MySummer(limit));
        }

        var threadList = summers.stream().map(Thread::new).toList();

        threadList.forEach(Thread::start);
        threadList.forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e) {
                //whatever
            }
        });

        myMutable = summers.stream().map(MySummer::reportValue).mapToInt(i -> i).sum();
    }


}
