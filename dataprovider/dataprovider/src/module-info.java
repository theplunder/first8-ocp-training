module dataprovider
{
    requires energydata;

    provides nl.first8.ocpklas.energydata.databron.EnergyAanbodProvider
            with nl.first8.ocpklas.dataprovider.impl.EnergyDataProviderImpl;

}