package nl.first8.ocpklas.dataprovider.impl;

import nl.first8.ocpklas.energydata.databron.EnergyAanbodProvider;
import nl.first8.ocpklas.energydata.domain.AanbodPeriode;
import nl.first8.ocpklas.energydata.domain.EnergieAanbodEntry;
import nl.first8.ocpklas.energydata.domain.PeriodeType;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.Year;
import java.util.Arrays;
import java.util.List;

public class EnergyDataProviderImpl implements EnergyAanbodProvider {


    @Override
    public List<EnergieAanbodEntry> provide(){
        try {
            return read();
        } catch (IOException e) {
            System.out.println("Could not read file or something! oh Noes");
            throw new RuntimeException(e);
        }
    }

    private List<EnergieAanbodEntry> read() throws IOException {
        var lines = readLines();
        int kolommen = lines.get(0).length();
        //Eerste regel zijn de headers, die halen we weg dmv deze substring
        return lines.subList(1, lines.size()).stream()
                .map(line -> line.split(";", kolommen))
                .map(stringArr ->
                        Arrays.stream(stringArr)
                                .map(this::cleanQuotedString)
                                .toList())
                .map(this::buildEntry)
                .toList();
    }


    private String cleanQuotedString(String quotedString) {
        // VRAAG: Wat is het verschil tussen .isBlank() en .isEmpty() ?
        if (quotedString == null || quotedString.isBlank()) {
            return quotedString;
        }

        return quotedString.substring(1, quotedString.length() - 1);
    }

    private List<String> readLines() throws IOException {
        //Dit heb ik van stack overflow, schaam je niet als je niet alles onthoudt :-)
        InputStream input = getClass().getResourceAsStream("EnergieAanbod.csv");

        List<String> result;
        //DeepDive: De Readers gebruiken een zogenoemde "Decorator Pattern"-Design Pattern
        try (BufferedReader in = new BufferedReader(new InputStreamReader(input))) {
            //Note: in.lines() is een nl.first8.ocpklas.stream
            result = in.lines().map(String::strip).toList();
        }
        return result;
    }

    /*"ID";"Perioden";"BrutoProductie_1";"EigenVerbruikBijElektrProductie_2";"NettoProductieTotaal_3";
     * "Kernenergie_4";"BrandstoffenTotaal_5";"Kolen_6";"Olieproducten_7";"Aardgas_8";"Biomassa_9";
     * "OverigeBrandstoffenNietHernieuwbaar_10";"Waterkracht_11";"WindenergieTotaal_12";"WindenergieOpLand_13";
     * "WindenergieOpZee_14";"Zonnestroom_15";"OverigeBronnen_16";
     * "InvoerTotaal_17";
     * "Belgie_18";"Denemarken_19";"Duitsland_20";"VerenigdKoninkrijk_21";"Noorwegen_22";
     * "UitvoerTotaal_23";
     * "Belgie_24";"Denemarken_25";"Duitsland_26";"VerenigdKoninkrijk_27";"Noorwegen_28";
     * "Distributieverliezen_29";"NettoVerbruikBerekend_30"
     */

    private EnergieAanbodEntry buildEntry(List<String> line) {
        AanbodPeriode aanbodPeriode = aanbodPeriode(line.get(1));
        var entry = new EnergieAanbodEntry(line.get(0), aanbodPeriode);

        entry.setBrutoProductie(parseInt(line.get(2)));
        entry.setEigenVerbruikBijElektrProductie(parseInt(line.get(3)));
        entry.setNettoProductieTotaal(parseInt(line.get(4)));
        entry.setKernenergie(parseInt(line.get(5)));
        entry.setBrandstoffenTotaal(parseInt(line.get(6)));
        entry.setKolen(parseInt(line.get(7)));
        entry.setOlieproducten(parseInt(line.get(8)));
        entry.setAardgas(parseInt(line.get(9)));
        entry.setBiomassa(parseInt(line.get(10)));
        entry.setOverigeBrandstoffenNietHernieuwbaar(parseInt(line.get(11)));
        entry.setWaterkracht(parseInt(line.get(12)));
        entry.setWindenergieTotaal(parseInt(line.get(13)));
        entry.setWindenergieOpLand(parseInt(line.get(14)));
        entry.setWindenergieOpZee(parseInt(line.get(15)));
        entry.setZonnestroom(parseInt(line.get(16)));
        entry.setOverigeBronnen(parseInt(line.get(17)));
        entry.setInvoerTotaal(parseInt(line.get(18)));
        entry.setUitvoerTotaal(parseInt(line.get(24)));
        entry.setDistributieverliezen(parseInt(line.get(30)));
        entry.setNettoVerbruikBerekend(parseInt(line.get(31)));

        return entry;
    }

    private static int parseInt(String integer) {
        if (integer == null || integer.isBlank()) {
            return 0;
        }
        return Integer.parseInt(integer);
    }


    private AanbodPeriode aanbodPeriode(String string) {
        var yearStr = string.substring(0, 4);
        var jaar = Year.of(Integer.parseInt(yearStr));

        String periodTypeStr = string.substring(4, 6);
        PeriodeType type = switch (periodTypeStr) {
            case "JJ":
                yield PeriodeType.JAAR;
            case "KW":
                yield PeriodeType.KWARTAAL;
            case "MM":
                yield PeriodeType.MAAND;
            default:
                throw new IllegalStateException("Unexpected value: " + periodTypeStr);
        };

        String periodeNummerStr = string.substring(6);
        int periodeNummer = Integer.parseInt(periodeNummerStr);

        return new AanbodPeriode(jaar, type, periodeNummer);
    }



}
