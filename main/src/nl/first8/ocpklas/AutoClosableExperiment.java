package nl.first8.ocpklas;

import java.io.Closeable;
import java.io.IOException;

public class AutoClosableExperiment {

    static class Test123 implements Closeable {

        String id = "";

        public Test123(String id) {
            this.id = id;
        }

        @Override
        public void close() throws IOException {
            System.out.println("I am closeing !" + id);
            throw  new IOException("lalalalalaaaaaaa!" + id);
        }
    }


    public static void main(String[] args) {
        try (Test123 test1 = new Test123("one");
             Test123 test2 = new Test123("two") ) {
            System.out.println("I am iun try");
        } catch (Exception e) {
            System.out.println("I am in Catch");
            System.out.println(e.getMessage());
        } finally {
            System.out.println("I am in finally");
        }


    }

}

