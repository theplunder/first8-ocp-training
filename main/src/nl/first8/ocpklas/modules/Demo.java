package nl.first8.ocpklas.modules;

//IntelliJ will gladly help you import it, but still doesn't work
//import nl.first8.ocpklas.energydata.internals.InternalStuff;

import nl.first8.ocpklas.energydata.api.ExternalStuff;
import nl.first8.ocpklas.energydata.api.ExternalStuff2;

public class Demo {

    public static void main(String[] args) {
        can_access_exported_packages();
        can_not_access_exported_packages();
        can_access_transitive_dependency();
    }



    private static void can_access_exported_packages() {
        var test = new ExternalStuff();
        test.printMe();
    }

    /**
     * Not possible, the package this class belongs to is not exported     *
     */
    private static void can_not_access_exported_packages() {
      // var test = new InternalStuff();
    }

    /* "TransitiveClass" accessible because we set  requires transitive transitives; in module-info of the energydata module
     * Good explanation here: https://stackoverflow.com/questions/46502453/whats-the-difference-between-requires-and-requires-transitive-statements-in-jav
     */
    private static void can_access_transitive_dependency() {
        var test = new ExternalStuff2();
        test.getTransitive().printMe();
    }

}
