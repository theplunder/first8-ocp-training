package nl.first8.ocpklas.stream;

import nl.first8.ocpklas.energydata.domain.EnergieAanbodEntry;
import nl.first8.ocpklas.energydata.databron.EnergyAanbodProvider;

import java.util.List;
import java.util.ServiceLoader;

public class StreamOefeningen {


    public static void main(String[] args) {
        EnergyAanbodProvider provider = ServiceLoader.load(EnergyAanbodProvider.class).findFirst().orElseThrow();

        List<EnergieAanbodEntry> list = provider.provide();

        // Probeer eens wat vragen te beantwoorden mbv streams
        // Bijvoorbeeld:
        // Hoeveel entries hebben we totaal?
        // Hoeveel jaren hebben we totaal?
        // Welke jaren hebben rapportages op maand basis?

        // Vanaf 1976 hebben we maandelijkse rapportages.
        // Welke jaar vanaf 1976 en maand hadden we de hoogste windenergie productie?
        // Hebben we in het jaar 2012 meer of minder energie uit hernieuwbare bronnen gebruikt dan in 2022?

    }


}
