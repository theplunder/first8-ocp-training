package nl.first8.ocpklas.serialize;

import java.io.ObjectInputStream;
import java.io.Serial;
import java.io.Serializable;

public class Book implements Serializable {


    private static final long serialVersionUID = -2936687026040726549L;
    private String bookName;
    private transient String description;
    private transient int copies;


    public Book() {
        System.out.println("I AM CALLED!");
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCopies() {
        return copies;
    }

    public void setCopies(int copies) {
        this.copies = copies;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookName='" + bookName + '\'' +
                ", description='" + description + '\'' +
                ", copies=" + copies +
                '}';
    }

    @Serial
    private void readObject(ObjectInputStream stream) {
        System.out.println("READOBJECT CALLED!");
        copies = -23423;
    }

    static {
        System.out.println("THIS IS A PROBLEM!");

    }

}
