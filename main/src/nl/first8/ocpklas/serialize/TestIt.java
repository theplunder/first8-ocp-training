package nl.first8.ocpklas.serialize;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class TestIt {
    public static void main(String[] args) throws Exception {

        Book book = new Book();
        book.setBookName("my book");
        book.setDescription("description");
        book.setCopies(112321);
        System.out.println(book);
        serialize(book, "filename");
        var book2 = deserialize("filename");
        System.out.println(book2);

    }

    public static void serialize(Book book, String fileName) throws Exception {
        FileOutputStream file = new FileOutputStream(fileName);
        ObjectOutputStream out = new ObjectOutputStream(file);
        out.writeObject(book);
        out.close();
        file.close();
    }

    public static Book deserialize(String fileName) throws Exception {
        FileInputStream file = new FileInputStream(fileName);
        ObjectInputStream in = new ObjectInputStream(file);
        Book book = (Book) in.readObject();
        in.close();
        file.close();
        return book;
    }
}
