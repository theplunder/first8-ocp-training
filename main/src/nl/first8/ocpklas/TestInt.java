package nl.first8.ocpklas;

import java.util.Arrays;
import java.util.List;

public class TestInt {


    public static void main(String[] args) {
        System.out.println(Integer.toBinaryString(3));


        System.out.println(Integer.toBinaryString(-3));

        System.out.println(Integer.toBinaryString(-4));

        System.out.println(Integer.toBinaryString(-1));

        System.out.println(Integer.toBinaryString(Integer.MIN_VALUE));
        System.out.println(Integer.toBinaryString(Integer.MAX_VALUE));

        var list = List.of(1,2,3);
        var list2 = List.of(1,3,6);
        System.out.println("-------");

        System.out.println(Arrays.mismatch(list.toArray(), list2.toArray()));
        System.out.println(Arrays.mismatch(list.toArray(), List.of(1,2,6).toArray()));
        System.out.println(Arrays.mismatch(list.toArray(), List.of(1,2,3).toArray()));
    }
}

