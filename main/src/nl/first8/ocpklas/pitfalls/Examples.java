package nl.first8.ocpklas.pitfalls;

import java.time.Month;
import java.util.ArrayList;
import java.util.List;

public class Examples {


    public static void main(String[] args) {
        numberEquals();
        System.out.println("------");
        leading0s();
        System.out.println("------");
        immutableOrNot();

        System.out.println("true? " + (128 == Long.valueOf(128)));
        System.out.println("true? " + (128 == 128L));

    }


    private static void numberEquals() {
        //Longs
        printEqualTest(24L, 24L);
        printEqualTest(127L, 127L);
        //Maar nu! Waarom?
        printEqualTest(128L, 128L);

        //Doubles
        printEqualTest(1.0, 1.0);

        Double dbl = Double.valueOf(1.1);
        printEqualTest(dbl, dbl);
    }

    //NOTE: worden automatisch "geboxed"
    private static void printEqualTest(Number number, Number otherNumber) {
        System.out.println(number + " is equal to " + otherNumber + "? " + (number == otherNumber));
    }


    private static void leading0s() {
        System.out.println(Month.of(07));

        // -> Hey! waarom compiled dit niet?
        //System.out.println(Month.of(08));
    }

    private static void immutableOrNot() {
        List<String> listOne = new ArrayList<>();
        listOne.add("a");
        listOne.add("b");
        listOne.add("c");
        var listTwo = List.of("a", "b", "c");

        try {
            listOne.add("d");
            listTwo.add("d");
        } catch (Exception e) {
            System.out.println("An expection \"" + e + "\" was thrown! Why?");

        }
    }
}
