package nl.first8.ocpklas.objectoriented;

import java.util.Comparator;
import java.util.function.Predicate;

public class NestedClasses {

    private int value;


    public static class StaticInner {
        private int otherValue;

        public void printValue(){
            // System.out.println(value); //ERROR: static, dus geen toegang tot 'value'
        }
    }

    public class Nested {
        private int otherValue;

        public void printValue(){
            System.out.println(value);

        }

    }


    public AnInterface buildAnonymous() {

        return new AnInterface() {
            @Override
            public long doSomething() {
                return 0;
            }
        }; //Ik heb geen bijbehorende class

    }

// Een oversimplistisch voorbeeld van een usecase van een anonymous class.
    public Predicate<Integer> biggerThanPredicate(int value) {
        return new Predicate<Integer>() {
            @Override
            public boolean test(Integer integer) {
                return integer > value;
            }
        };
    }


}
