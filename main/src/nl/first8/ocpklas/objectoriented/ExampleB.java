package nl.first8.ocpklas.objectoriented;

public class ExampleB {


    public class Vehicle { //extends Object
        public void move() {
            //...
        }
    }

    public class Airplane extends Vehicle {

        public void takeOff() {
            //...
        }

        @Override
        public String toString() {
            return "I'm an Airplane!";
        }
    }


}
