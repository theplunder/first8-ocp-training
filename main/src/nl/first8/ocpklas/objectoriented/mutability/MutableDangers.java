package nl.first8.ocpklas.objectoriented.mutability;

import java.util.*;

public class MutableDangers {


    public static void main(String[] args) {

        Mutable mutable = new Mutable(10);
        Map<Mutable, Integer> map = new HashMap<>();
        Set<Mutable> set = new HashSet<>();

        map.put(mutable, 99);
        set.add(mutable);

        System.out.println(map.get(mutable)); //should print 99

        set.add(mutable);
        System.out.println(set.size()); //should print 1


        System.out.println("Now we mutate ----");
        mutable.setValue(9999);

        System.out.println(map.get(mutable)); //now it can not longer find the value!

        set.add(mutable);
        System.out.println(set.size()); //It added the same object again!


//
//        //Left as an exercise to the reader
//        SortedSet<Mutable> sortedSet = new TreeSet<>();

    }



    private static class Mutable implements Comparable<Integer> {

        private int value;

        public Mutable(int value) {
            this.value = value;
        }

        @Override
        public int compareTo(Integer o) {
            return Integer.compare(this.value, o);
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Mutable mutable = (Mutable) o;
            return value == mutable.value;
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }

    }


}
