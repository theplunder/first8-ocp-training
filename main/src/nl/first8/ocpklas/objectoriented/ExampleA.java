package nl.first8.ocpklas.objectoriented;

public class ExampleA {

    abstract class Organism {}

    abstract class Animal extends Organism {}

    abstract class Mammal extends Animal {}

    abstract class Primate extends Mammal {}

    class Human extends Primate{}



    private void bla(){
        var human = new Human();
    }
}
