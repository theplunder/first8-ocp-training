package nl.first8.ocpklas.transitives;

public class TransitiveClass {

    public static void main(String[] args) {

    }

    public void printMe(){
        Class<?> caller = StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE).getCallerClass();
        System.out.println("I " + this.getClass() + " was accessed  from  " + caller.getName());
    }

}
