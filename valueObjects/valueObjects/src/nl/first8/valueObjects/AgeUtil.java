package nl.first8.valueObjects;

import java.time.LocalDate;

public class AgeUtil {

    public static int ageDifference(LocalDate first, LocalDate second) {
        return first.until(second).getYears();
    }

    public static int getAge(Person person) {
        var now = LocalDate.now();
        return person.getBirthdate().until(now).getYears();
    }

}
