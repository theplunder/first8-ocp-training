package nl.first8.valueObjects;

import java.time.LocalDate;

public class Person {

    private final String firstName;
    private final String lastName;
    private final String infix; //bv van, de, der, ...
    private LocalDate birthdate;

    private String birthCountry;
    private String birthCity;

    private String currentCountry;
    private String currentCity;
    private String street;
    private Integer houseNumber;
    private String houseNumberPosftix; //e.g. bis, -II , ...
    private String postalCode;


    public Person(String firstName, String lastName, String infix) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.infix = infix;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public String getBirthCountry() {
        return birthCountry;
    }

    public void setBirthCountry(String birthCountry) {
        this.birthCountry = birthCountry;
    }

    public String getBirthCity() {
        return birthCity;
    }

    public void setBirthCity(String birthCity) {
        this.birthCity = birthCity;
    }

    public String getCurrentCountry() {
        return currentCountry;
    }

    public void setCurrentCountry(String currentCountry) {
        this.currentCountry = currentCountry;
    }

    public String getCurrentCity() {
        return currentCity;
    }

    public void setCurrentCity(String currentCity) {
        this.currentCity = currentCity;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(Integer houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getHouseNumberPosftix() {
        return houseNumberPosftix;
    }

    public void setHouseNumberPosftix(String houseNumberPosftix) {
        this.houseNumberPosftix = houseNumberPosftix;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
}
